define(["require", "exports", "lib/silica", "classes/Turtle"], function (require, exports, SilicaTS, Turtle) {
    var State = SilicaTS.State;
    var Game = SilicaTS.Game;
    var Vector2 = SilicaTS.Vector2;
    var AppMain = (function () {
        function AppMain() {
        }
        AppMain.prototype.Run = function () {
            var s = new State();
            var game = new Game(document.body, s);
            game.stepDraw = true;
            game.Start();
            var t = BasicTurtles.PythagorasTree();
            s.AddChild(t);
            var itt = 0;
            var commandString = t.Rewrite(itt);
            t.ExecuteCommand(commandString);
            game.ShowUI();
            game.Draw();
            game.Frame.AddButton("Next step", function () {
                t.magnitude *= t.magnitudeMultiplier;
                commandString = t.Rewrite(++itt);
                t.ExecuteCommand(commandString);
                game.Draw();
            });
            game.Frame.AddButton("Previous step", function () {
                if (itt > 0) {
                    t.magnitude /= t.magnitudeMultiplier;
                    commandString = t.Rewrite(--itt);
                    t.ExecuteCommand(commandString);
                }
                game.Draw();
            });
            var select = game.Frame.AddDropdownList("Prefabs", BasicTurtles.prefabNames);
            select.onchange = function () {
                s.RemoveChild(t);
                var temp = t;
                t = BasicTurtles.prefabTurtles[select.selectedIndex];
                t.logCommand = temp.logCommand;
                temp.Reset();
                delete temp;
                t.Reset(true);
                s.AddChild(t);
                itt = 0;
                commandString = t.Rewrite(itt);
                t.ExecuteCommand(commandString);
                game.Draw();
            };
            game.Frame.AddInput("checkbox", "Log result in console", "false", function (ev) {
                var target = ev.target;
                t.logCommand = target.checked;
            });
        };
        return AppMain;
    })();
    exports.AppMain = AppMain;
    var BasicTurtles = (function () {
        function BasicTurtles() {
        }
        BasicTurtles.PythagorasTree = function () {
            var t = new Turtle(new Vector2(450, 470), new Vector2(0, -1), 400);
            t.SetAngle(45);
            t.AddRewriteRule("0", "1[0]0");
            t.AddRewriteRule("1", "11");
            var op0 = function () {
                var mag = t.magnitude;
                t.magnitude *= .25;
                t.Forward();
                t.magnitude = mag;
            };
            t.AddOperation("0", op0);
            t.AddOperation("1", t.Forward);
            var op = function () {
                t.Push();
                t.Rotate(true);
            };
            var op2 = function () {
                t.Pop();
                t.Rotate();
            };
            t.AddOperation("[", op);
            t.AddOperation("]", op2);
            t.axiom = "0";
            return t;
        };
        BasicTurtles.KochSnoflake = function () {
            var t = new Turtle(new Vector2(250, 150), new Vector2(1, 0), 100);
            t.SetAngle(60);
            t.AddRewriteRule("F", "F-F++F-F");
            t.magnitudeMultiplier = .45;
            t.AddOperation("F", t.Forward);
            t.AddOperation("+", t.Rotate);
            t.AddOperation("-", function () {
                t.Rotate(true);
            });
            t.axiom = 'F++F++F';
            return t;
        };
        BasicTurtles.SierpinskiTriangle = function () {
            var t = new Turtle(new Vector2(250, 400), new Vector2(1, 0), 400);
            t.SetAngle(60);
            t.AddRewriteRule("A", "B-A-B");
            t.AddRewriteRule("B", "A+B+A");
            t.AddOperation('A', t.Forward);
            t.AddOperation('B', t.Forward);
            t.AddOperation('+', t.Rotate);
            t.AddOperation('-', function () {
                t.Rotate(true);
            });
            t.axiom = "A";
            return t;
        };
        BasicTurtles.DragonCurve = function () {
            var t = new Turtle(new Vector2(400, 400), new Vector2(1, 0), 150);
            t.magnitudeMultiplier = .75;
            t.SetAngle(90);
            t.AddRewriteRule('X', "X+YF+");
            t.AddRewriteRule('Y', "-FX-Y");
            t.AddOperation('X', function () {
                return null;
            });
            t.AddOperation('Y', function () {
                return null;
            });
            t.AddOperation('F', t.Forward);
            t.AddOperation('+', t.Rotate);
            t.AddOperation('-', function () {
                t.Rotate(true);
            });
            t.axiom = "FX";
            return t;
        };
        BasicTurtles.FractalPlant = function () {
            var t = new Turtle(new Vector2(100, 470), new Vector2(0.5, -0.5), 100);
            t.thickness = 1;
            t.SetAngle(25);
            t.AddRewriteRule("X", "F-[[X]+X]+F[+FX]-X");
            t.AddRewriteRule("F", "FF");
            t.AddOperation('X', function () {
                return;
            });
            t.AddOperation('F', t.Forward);
            t.AddOperation('[', t.Push);
            t.AddOperation(']', t.Pop);
            t.AddOperation('+', t.Rotate);
            t.AddOperation("-", function () {
                t.Rotate(true);
            });
            t.axiom = "F-[[X]+X]+F[+FX]-X";
            t.style = "black";
            return t;
        };
        BasicTurtles.prefabNames = ['Pythagoras Tree', 'Koch Snowflake', 'Sierpinski', 'Dragon Curve', 'Fractal Plant'];
        BasicTurtles.prefabTurtles = [BasicTurtles.PythagorasTree(), BasicTurtles.KochSnoflake(), BasicTurtles.SierpinskiTriangle(), BasicTurtles.DragonCurve(), BasicTurtles.FractalPlant()];
        return BasicTurtles;
    })();
    exports.BasicTurtles = BasicTurtles;
});
//# sourceMappingURL=AppMain.js.map