﻿import SilicaTS = require("../lib/silica");
import Entity = SilicaTS.Entity;
import Vector2 = SilicaTS.Vector2;
import Stack = require("classes/Stack");

export = Turtle;

class Turtle extends Entity {
    public style:any;
    
    public operations: { [operator: string]: () => void } = {};
    public rewriteRules: { [lefthand: string]: string } = {};
    public thickness:number = 1.5;
    public axiom: string;
    public magnitudeMultiplier = .5;
    public logCommand: boolean;
    
    private lines: Array<Line>;
    private positionStack: Stack<Vector2>;
    private headingStack: Stack<Vector2>;

    private originalPos: Vector2;
    private originalHeading: Vector2;
    private originalMagnitude;
    private angle: number;

    constructor(public position: Vector2, public heading: Vector2, public magnitude) {
        super(position, SilicaTS.Vector2.Zero);
        this.originalPos = position;
        this.originalHeading = heading;
        this.originalMagnitude = magnitude;

        this.lines = new Array<Line>();
        this.positionStack = new Stack<Vector2>();
        this.headingStack = new Stack<Vector2>();
    }

    public AddRewriteRule(lhd: string, rhd: string) {
        this.rewriteRules[lhd] = rhd;
    }

    public SetAngle(degrees: number) {
        this.angle = degrees * (Math.PI / 180);
    }

    public AddOperation(operator: string, operation: () => void) {
        this.operations[operator] = operation;
    }

    public Reset(includeMagnitude:boolean = false) {
        this.heading = this.originalHeading;
        this.position = this.originalPos;
        this.lines = new Array<Line>();
        this.positionStack = new Stack<Vector2>();
        this.headingStack = new Stack<Vector2>();

        if (!includeMagnitude) return;
        this.magnitude = this.originalMagnitude;
    }

    public ExecuteCommand(command: string) {
        this.Reset();
        for (var i = 0; i < command.length; i++) {

            this.operations[command[i]].call(this);
        }
        if (this.logCommand)
            console.log(command);
    }
    public Rewrite(depth: number, input:string = this.axiom) {
        if (depth == 0)
            return input;
        var retval = "";
        for (var i = 0; i < input.length; i++) {
            var c = input[i];
            var rewrite = this.rewriteRules[c];
            retval += rewrite != null ? rewrite : c;
        }
        return this.Rewrite(--depth, retval);
    }
    public Rotate(negative: boolean = false) {
        var theta = (negative) ? -this.angle : this.angle;
        var v = this.heading;

        var cos = Math.cos(theta);
        var sin = Math.sin(theta);
        var x = (v.x * cos - v.y * sin);
        var y = (v.x * sin + v.y * cos);
        this.heading = new Vector2(x, y).Normalized();
    }

    public Push() {
        this.headingStack.Push(this.heading);
        this.positionStack.Push(this.position);
    }

    public Pop() {
        this.heading = this.headingStack.Pop();
        this.position = this.positionStack.Pop();
    }

    public Update() {

    }

    public Forward() {
        var mag = this.heading.MultiplyScalar(this.magnitude);

        var newpos = this.position.Add(mag);
        this.lines.push(new Line(this.position, newpos));
        this.position = newpos;
    }

    public Draw(context: CanvasRenderingContext2D): void {
        context.beginPath();
        context.strokeStyle = this.style;
        context.lineWidth = this.thickness;
        this.lines.forEach((l) => {
            context.moveTo(l.start.x, l.start.y);
            context.lineTo(l.end.x, l.end.y);
        });
        context.stroke();
        context.closePath();
    }

    
}

class Line {
    constructor(public start: Vector2, public end: Vector2) {

    }
}