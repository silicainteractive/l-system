﻿
export = Stack;

class Stack<T> {
    private data: Array<T>;

    constructor() {
        this.data = new Array<T>();
    }

    public Pop(): T {
        return this.data.pop();
    }

    public Push(item: T): void {
        this.data.push(item);
    }
} 