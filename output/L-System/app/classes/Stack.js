define(["require", "exports"], function (require, exports) {
    var Stack = (function () {
        function Stack() {
            this.data = new Array();
        }
        Stack.prototype.Pop = function () {
            return this.data.pop();
        };
        Stack.prototype.Push = function (item) {
            this.data.push(item);
        };
        return Stack;
    })();
    return Stack;
});
//# sourceMappingURL=Stack.js.map