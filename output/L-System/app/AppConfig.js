/// <reference path="../modules/require.d.ts" />
/// <reference path="AppMain.ts" />
require.config({});
require(['AppMain'], function (main) {
    App = new main.AppMain();
    App.Run();
});
//# sourceMappingURL=AppConfig.js.map