var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function (require, exports) {
    var Entity = (function () {
        function Entity(position, size) {
            this.position = position;
            this.size = size;
        }
        Entity.prototype.OnClick = function (event) {
        };
        Entity.prototype.CentrePoint = function () {
            var x = this.position.x + this.size.x / 2;
            var y = this.position.y + this.size.y / 2;
            return new Vector2(x, y);
        };
        Entity.prototype.ContainsPoint = function (point) {
            var test1 = point.x >= this.position.x && point.x <= this.position.x + this.size.x;
            var test2 = point.y >= this.position.y && point.y <= this.position.y + this.size.y;
            return test1 && test2;
        };
        Entity.prototype.Update = function () {
            throw new Error("Abstract!");
        };
        Entity.prototype.Draw = function (context) {
            throw new Error("Abstract!");
        };
        return Entity;
    })();
    exports.Entity = Entity;
    var Game = (function () {
        function Game(container, startingState) {
            this.container = container;
            this.currentFPS = 0;
            this.lastTime = new Date();
            this.renderer = new Renderer(container, container.offsetWidth, container.offsetHeight);
            this.Frame = new SilicaFrame(container);
            this.Frame.Hide();
            this.SwitchState(startingState);
            this.isActive = true;
            this.gametime = this.startTime = new Date();
            Input.Initialize(this.renderer.canvas);
        }
        Game.prototype.ShowUI = function () {
            this.Frame.Show();
        };
        Game.prototype.HideUI = function () {
            this.Frame.Hide();
        };
        Game.prototype.Start = function () {
            this.interval = setInterval(this.Update.bind(this), 1000 / this.fps);
        };
        Game.prototype.SetScale = function (x, y) {
            this.renderer.context.scale(x, y);
            this.scaleValue = new Vector2(x, y);
        };
        Game.prototype.GetScale = function () {
            return this.scaleValue;
        };
        Game.prototype.SetFPS = function (newFPS) {
            this.fps = newFPS;
            clearInterval(this.interval);
            this.interval = setInterval(this.Update.bind(this), 1000 / this.fps);
        };
        Game.prototype.Scale = function (s) {
            this.renderer.context.scale(s, s);
        };
        Game.prototype.SwitchState = function (newState) {
            this.currentState = newState;
            this.renderer.canvas.onclick = this.currentState.OnClick.bind(this.currentState);
        };
        Game.prototype.Update = function () {
            if (!this.stepDraw) {
                this.Draw();
                this.currentState.Update();
            }
            if (this.debug) {
                this.gametime = new Date();
                this.currentFPS = 1000 / (this.gametime.getMilliseconds() - this.lastTime.getMilliseconds());
                this.lastTime = this.gametime;
            }
        };
        Game.prototype.Draw = function () {
            this.renderer.Clear();
            //this.renderer.DebugDraw(this.currentFPS);
            this.currentState.Draw(this.renderer.context);
        };
        return Game;
    })();
    exports.Game = Game;
    var Input = (function () {
        function Input(canvas) {
            this.canvas = canvas;
            this.pressedKeys = [];
            document.onkeydown = this.OnKeyDown.bind(this);
            document.onkeyup = this.OnKeyUp.bind(this);
            document.onkeypress = this.OnKeyPress.bind(this);
        }
        Input.Initialize = function (canvas) {
            Input.instance = new Input(canvas);
        };
        Input.prototype.OnKeyDown = function (event) {
            this.pressedKeys.push(event.keyCode);
        };
        Input.prototype.OnKeyUp = function (event) {
            Input.instance.pressedKeys = [];
        };
        Input.prototype.OnKeyPress = function (event) {
        };
        Input.IsKeyDown = function (key) {
            var b = Input.instance.pressedKeys.every(function (k) {
                if (k == key)
                    return false;
                return true;
            });
            return !b;
        };
        Input.GetMousePos = function (event) {
            var x = event.x - Input.instance.canvas.offsetLeft + window.pageXOffset;
            var y = event.y - Input.instance.canvas.offsetTop + window.pageYOffset;
            return new Vector2(x, y);
        };
        return Input;
    })();
    exports.Input = Input;
    (function (Keys) {
        Keys[Keys["NUM0"] = 48] = "NUM0";
        Keys[Keys["NUM1"] = 49] = "NUM1";
        Keys[Keys["NUM2"] = 50] = "NUM2";
        Keys[Keys["NUM3"] = 51] = "NUM3";
        Keys[Keys["NUM4"] = 52] = "NUM4";
        Keys[Keys["NUM5"] = 53] = "NUM5";
        Keys[Keys["NUM6"] = 54] = "NUM6";
        Keys[Keys["NUM7"] = 55] = "NUM7";
        Keys[Keys["NUM8"] = 56] = "NUM8";
        Keys[Keys["NUM9"] = 57] = "NUM9";
        Keys[Keys["A"] = 65] = "A";
        Keys[Keys["B"] = 66] = "B";
        Keys[Keys["C"] = 67] = "C";
        Keys[Keys["D"] = 68] = "D";
        Keys[Keys["E"] = 69] = "E";
        Keys[Keys["F"] = 70] = "F";
        Keys[Keys["G"] = 71] = "G";
        Keys[Keys["H"] = 72] = "H";
        Keys[Keys["I"] = 73] = "I";
        Keys[Keys["J"] = 74] = "J";
        Keys[Keys["K"] = 75] = "K";
        Keys[Keys["L"] = 76] = "L";
        Keys[Keys["M"] = 77] = "M";
        Keys[Keys["N"] = 78] = "N";
        Keys[Keys["O"] = 79] = "O";
        Keys[Keys["P"] = 80] = "P";
        Keys[Keys["Q"] = 81] = "Q";
        Keys[Keys["R"] = 82] = "R";
        Keys[Keys["S"] = 83] = "S";
        Keys[Keys["T"] = 84] = "T";
        Keys[Keys["V"] = 85] = "V";
        Keys[Keys["U"] = 86] = "U";
        Keys[Keys["W"] = 87] = "W";
        Keys[Keys["X"] = 88] = "X";
        Keys[Keys["Y"] = 89] = "Y";
        Keys[Keys["Z"] = 90] = "Z";
    })(exports.Keys || (exports.Keys = {}));
    var Keys = exports.Keys;
    var Renderer = (function () {
        function Renderer(container, width, height) {
            this.container = container;
            this.width = width;
            this.height = height;
            this.canvas = document.createElement("canvas");
            this.context = this.canvas.getContext("2d");
            this.canvas.width = width;
            this.canvas.height = height;
            this.canvas.style.position = "absolute";
            container.appendChild(this.canvas);
        }
        Renderer.prototype.DebugDraw = function (fps) {
            this.context.font = "16px Arial";
            this.context.fillText(fps.toPrecision(2).toString(), 10, 15);
            console.log(fps);
        };
        Renderer.prototype.Clear = function () {
            this.context.clearRect(0, 0, 2000, 2000);
        };
        return Renderer;
    })();
    exports.Renderer = Renderer;
    var Sprite = (function (_super) {
        __extends(Sprite, _super);
        function Sprite(position, size) {
            _super.call(this, position, size);
            this.updateEvents = [];
        }
        Sprite.prototype.LoadGrapic = function (location) {
            this.image = document.createElement("img");
            this.image.src = location;
            console.log(this.image.width);
        };
        Sprite.prototype.Update = function () {
            this.updateEvents.forEach(function (f) {
                f();
            });
        };
        Sprite.prototype.Draw = function (graphics) {
            graphics.drawImage(this.image, 32, 0, 32, 32, this.position.x, this.position.y, 32, 32);
        };
        return Sprite;
    })(Entity);
    exports.Sprite = Sprite;
    var State = (function () {
        function State() {
            this.children = new Array();
        }
        State.prototype.AddChild = function (child) {
            this.children.push(child);
        };
        State.prototype.RemoveChild = function (child) {
            var m = this.children.lastIndexOf(child);
            delete this.children[m];
        };
        State.prototype.Create = function () {
            throw new Error("ABSTRACT");
        };
        State.prototype.Update = function () {
            this.children.forEach(function (c) {
                c.Update();
            });
        };
        State.prototype.OnClick = function (event) {
            this.children.forEach(function (c) {
                c.OnClick(event);
            });
        };
        State.prototype.Draw = function (context) {
            this.children.forEach(function (c) {
                c.Draw(context);
            });
        };
        return State;
    })();
    exports.State = State;
    var MathHelper = (function () {
        function MathHelper() {
            this.TAU = Math.PI * 2;
        }
        MathHelper.Lerp = function (a, b, t) {
            return a + (b - a) * t;
        };
        return MathHelper;
    })();
    exports.MathHelper = MathHelper;
    var Matrix2X2 = (function () {
        function Matrix2X2() {
            this.values = new Array(2);
        }
        Matrix2X2.RotaionMatrix = function (theta) {
            var retval = new Matrix2X2();
            var m0 = Math.cos(theta);
            var m2 = Math.sin(theta);
            var m1 = -m2;
            retval.values[0] = new Array(m0, m1);
            retval.values[1] = new Array(m2, m0);
            return retval;
        };
        return Matrix2X2;
    })();
    exports.Matrix2X2 = Matrix2X2;
    var Vector2 = (function () {
        function Vector2(x, y) {
            this.x = x;
            this.y = y;
            this.length = null;
        }
        Vector2.prototype.Add = function (v) {
            return new Vector2(this.x + v.x, this.y + v.y);
        };
        Vector2.prototype.Subtract = function (v) {
            return new Vector2(this.x - v.x, this.y - v.y);
        };
        Vector2.prototype.MultiplyScalar = function (n) {
            return new Vector2(this.x * n, this.y * n);
        };
        Vector2.prototype.DivideScalar = function (n) {
            return new Vector2(this.x / n, this.y / n);
        };
        Vector2.prototype.MultiplyVector = function (v) {
            return new Vector2(this.x * v.x, this.y * v.y);
        };
        Vector2.prototype.Rotate = function (theta) {
            var cosT = Math.cos(theta);
            var sinT = Math.sin(theta);
            var x = this.x * cosT - this.y * sinT;
            var y = this.x * sinT + this.y * cosT;
            return new Vector2(x, y);
        };
        Vector2.prototype.Length = function () {
            if (this.length != null)
                return this.length;
            this.length = (Math.sqrt(this.x * this.x + this.y * this.y));
            return this.length;
        };
        Vector2.prototype.MultiplyMatrix = function (matrix) {
            return new Vector2(this.x * matrix.values[0][0] - this.y * -matrix.values[0][1], this.x * matrix.values[1][0] + this.y * matrix.values[1][1]);
        };
        Vector2.prototype.toString = function () {
            return "[" + this.x + ", " + this.y + "]";
        };
        Vector2.prototype.Normalized = function () {
            return this.DivideScalar(this.Length());
        };
        Vector2.prototype.Copy = function () {
            return new Vector2(this.x, this.y);
        };
        Vector2.Zero = new Vector2(0, 0);
        Vector2.Up = new Vector2(0, 1);
        Vector2.Right = new Vector2(1, 0);
        return Vector2;
    })();
    exports.Vector2 = Vector2;
    var Frame = (function () {
        function Frame(container) {
            this.container = container;
            this.ui = document.createElement("div");
            this.width = container.offsetWidth;
            this.height = container.offsetHeight;
        }
        Frame.prototype.AppendChild = function (child) {
            this.ui.appendChild(child);
            this.ui.style.height = (this.ui.offsetHeight + child.offsetHeight + 5) + "px";
        };
        Frame.prototype.AppendAt = function (index, child) {
            this.ui.insertBefore(child, this.ui.children.item(index));
            this.ui.style.height = (this.ui.offsetHeight + child.offsetHeight + 5) + "px";
        };
        Frame.prototype.Show = function () {
            this.container.appendChild(this.ui);
        };
        Frame.prototype.Hide = function () {
            this.container.removeChild(this.ui);
        };
        Frame.prototype.AddFrame = function () {
            var frame = new Frame(this.ui);
            return frame;
        };
        Frame.prototype.AddButtonAt = function (index, text, callback) {
            var b = document.createElement("button");
            b.className = "UIElement";
            b.style.position = "relative";
            b.textContent = text;
            b.onclick = callback.bind(this);
            b.style.width = "100%";
            this.AppendAt(index, b);
        };
        Frame.prototype.AddButton = function (text, callback) {
            var b = document.createElement("button");
            b.className = "UIElement";
            b.style.position = "relative";
            b.textContent = text;
            b.onclick = callback.bind(this);
            b.style.width = "100%";
            this.AppendChild(b);
        };
        Frame.prototype.AddNumberInput = function (labelText, defaultNumber, onChangeCallback) {
            var uiDiv = document.createElement("div");
            var label = document.createElement("label");
            var input = document.createElement("input");
            label.textContent = labelText;
            input.type = "number";
            input.value = defaultNumber.toString();
            input.style.height = uiDiv.style.height;
            input.onchange = onChangeCallback.bind(this);
            uiDiv.className = input.className = "UIElement";
            uiDiv.appendChild(label);
            uiDiv.appendChild(input);
            this.AppendChild(uiDiv);
        };
        Frame.prototype.AddInput = function (type, labelText, defaultValue, onChangeCallback) {
            var uiDiv = document.createElement("div");
            var label = document.createElement("label");
            var input = document.createElement("input");
            label.textContent = labelText;
            input.type = type;
            input.value = defaultValue;
            input.onchange = onChangeCallback.bind(this);
            uiDiv.className = input.className = "UIElement";
            uiDiv.appendChild(label);
            uiDiv.appendChild(input);
            this.AppendChild(uiDiv);
        };
        Frame.prototype.AddLabel = function (labelText) {
            var div = document.createElement("div");
            var label = document.createElement("label");
            label.textContent = labelText;
            div.appendChild(label);
            div.className = "UIElement";
            return div;
        };
        Frame.prototype.AddDropdownList = function (labelText, values) {
            var select = document.createElement("select");
            select.className = "UIElement";
            for (var i = 0; i < values.length; i++) {
                var option = document.createElement("option");
                option.value = values[i];
                option.text = option.value;
                select.add(option);
            }
            var div = this.AddLabel(labelText);
            div.appendChild(select);
            this.AppendChild(div);
            return select;
        };
        return Frame;
    })();
    exports.Frame = Frame;
    var SilicaFrame = (function (_super) {
        __extends(SilicaFrame, _super);
        function SilicaFrame(container) {
            _super.call(this, container);
            this.container = container;
            this.time = 0;
            this.ui.className = "UIFrame";
            this.open = true;
            var collapseButton = this.collapseButton = document.createElement("button");
            collapseButton.className = "UIElement";
            collapseButton.style.position = "absolute";
            collapseButton.style.width = "100%";
            collapseButton.style.height = (this.height / 25).toString() + "px";
            collapseButton.style.bottom = "0";
            collapseButton.textContent = "Close UI";
            this.ui.appendChild(collapseButton);
            collapseButton.onclick = this.OpenClose.bind(this);
            this.openPosition = this.ui.offsetTop;
            this.closedPostion = this.openPosition - collapseButton.offsetTop;
            this.Show();
        }
        SilicaFrame.prototype.AppendChild = function (child) {
            _super.prototype.AppendChild.call(this, child);
            this.closedPostion = this.openPosition - this.collapseButton.offsetTop;
        };
        SilicaFrame.prototype.OpenFrame = function () {
            this.ui.style.top = MathHelper.Lerp(this.closedPostion, this.openPosition, this.time) + "px";
            if (this.time >= 1) {
                cancelAnimationFrame(this.animID);
                this.time = 0;
                this.open = true;
            }
            else {
                this.time += .2;
                this.animID = requestAnimationFrame(this.OpenFrame.bind(this));
            }
        };
        SilicaFrame.prototype.CloseFrame = function () {
            this.ui.style.top = MathHelper.Lerp(this.openPosition, this.closedPostion, this.time) + "px";
            if (this.time >= 1) {
                cancelAnimationFrame(this.animID);
                this.time = 0;
                this.open = false;
            }
            else {
                this.time += .2;
                this.animID = requestAnimationFrame(this.CloseFrame.bind(this));
            }
        };
        SilicaFrame.prototype.OpenClose = function (ev) {
            if (this.open) {
                this.animID = requestAnimationFrame(this.CloseFrame.bind(this));
                this.collapseButton.textContent = "Open UI";
            }
            else {
                this.animID = requestAnimationFrame(this.OpenFrame.bind(this));
                this.collapseButton.textContent = "Close UI";
            }
        };
        return SilicaFrame;
    })(Frame);
    exports.SilicaFrame = SilicaFrame;
    var Window = (function (_super) {
        __extends(Window, _super);
        function Window(container) {
            var _this = this;
            _super.call(this, container);
            this.ui.className = "UIWindow";
            this.Show();
            var closeButton = document.createElement("button");
            closeButton.className = "UIElement";
            closeButton.style.position = "relative";
            closeButton.style.width = closeButton.style.height = (this.height / 40).toString() + "px";
            closeButton.style.left = "95%";
            closeButton.textContent = "X";
            closeButton.style.textAlign = "center";
            this.ui.appendChild(closeButton);
            closeButton.onclick = (function (ev) {
                _this.Hide();
            }).bind(this);
        }
        return Window;
    })(Frame);
    exports.Window = Window;
});
//# sourceMappingURL=silica.js.map