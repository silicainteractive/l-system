export interface IDictionary<K,V> {
    Add(key: K, value: V): void;
    Remove(key: K): void;
    ContainsKey(key: K): boolean;
    Keys(): K[];
    Values(): V[];
} 
export class Entity {
    constructor(public position: Vector2, public size: Vector2) {
    }
    public OnClick(event: MouseEvent) :void {
    }
    public CentrePoint():Vector2 {
        var x = this.position.x + this.size.x / 2;
        var y = this.position.y + this.size.y / 2;
        return new Vector2(x, y);
    }
    public ContainsPoint(point: Vector2): boolean {
        var test1 = point.x >= this.position.x && point.x <= this.position.x + this.size.x;
        var test2 = point.y >= this.position.y && point.y <= this.position.y + this.size.y;
        return test1 && test2;
    }
    public Update(): void { throw new Error("Abstract!"); }
    public Draw(context: CanvasRenderingContext2D): void { throw new Error("Abstract!"); }
}
export class Game {
    public renderer: Renderer;
    public currentState: State;
    public isActive: boolean;
    public fps: number;
    public currentFPS: number = 0;
    private startTime: Date; 
    public gametime: Date;
    public input: Input;
    public Frame: SilicaFrame;
    public stepDraw: boolean;
    public debug: boolean;
    private interval: number;
    private scaleValue: Vector2;
    constructor(public container: HTMLElement, startingState: State) {
        this.renderer = new Renderer(container, container.offsetWidth, container.offsetHeight);
        this.Frame = new SilicaFrame(container);
        this.Frame.Hide();
        this.SwitchState(startingState);
        this.isActive = true;
        this.gametime = this.startTime = new Date();
        Input.Initialize(this.renderer.canvas);
    }
    public ShowUI() {
        this.Frame.Show();
    }
    public HideUI() {
        this.Frame.Hide();
    }
    public Start() {
        this.interval = setInterval(this.Update.bind(this), 1000 / this.fps);
        }
    public SetScale(x: number, y: number) {
        this.renderer.context.scale(x, y);
        this.scaleValue = new Vector2(x, y);
    }
    public GetScale(): Vector2 {
        return this.scaleValue;
    }
    public SetFPS(newFPS: number) {
        this.fps = newFPS;
        clearInterval(this.interval);
        this.interval = setInterval(this.Update.bind(this), 1000 / this.fps);
    }
    public Scale(s: number) {
        this.renderer.context.scale(s,s);
    }
    public SwitchState(newState: State) {
        this.currentState = newState;
        this.renderer.canvas.onclick = this.currentState.OnClick.bind(this.currentState);
    }
    public lastTime = new Date();
    public Update() {
        if (!this.stepDraw) {
            this.Draw();
            this.currentState.Update();
        }
        if (this.debug) {
            this.gametime = new Date();
            this.currentFPS = 1000 / (this.gametime.getMilliseconds() - this.lastTime.getMilliseconds());
            this.lastTime = this.gametime;
        }
    }
    public Draw() {
        this.renderer.Clear();
        //this.renderer.DebugDraw(this.currentFPS);
        this.currentState.Draw(this.renderer.context);
    }
}
export class Input {
    private static instance: Input;
    private pressedKeys: Array<number>;
    constructor(public canvas: HTMLCanvasElement) {
        this.pressedKeys = [];
        document.onkeydown = this.OnKeyDown.bind(this);
        document.onkeyup = this.OnKeyUp.bind(this);
        document.onkeypress = this.OnKeyPress.bind(this);
    }
    public static Initialize(canvas: HTMLCanvasElement) {
        Input.instance = new Input(canvas);
    }
    private OnKeyDown(event: KeyboardEvent) {
        this.pressedKeys.push(event.keyCode);
    }
    private OnKeyUp(event: KeyboardEvent) {
        Input.instance.pressedKeys = [];
    }
    private OnKeyPress(event: KeyboardEvent) {
    }
    public static IsKeyDown(key: Keys) {
        var b = Input.instance.pressedKeys.every((k) => {
            if (k == key)
                return false;
            return true;
        });
        return !b;
    }
    public static GetMousePos(event: MouseEvent): Vector2 {
        var x = event.x - Input.instance.canvas.offsetLeft + window.pageXOffset;
        var y = event.y - Input.instance.canvas.offsetTop + window.pageYOffset;
        return new Vector2(x, y);
    }
} 
export enum Keys {
    NUM0 = 48,
    NUM1 = 49,
    NUM2 = 50,
    NUM3 = 51,
    NUM4 = 52,
    NUM5 = 53,
    NUM6 = 54,
    NUM7 = 55,
    NUM8 = 56,
    NUM9 = 57,
    A = 65,
    B = 66,
    C = 67,
    D = 68,
    E = 69,
    F = 70,
    G = 71,
    H = 72,
    I = 73,
    J = 74,
    K = 75,
    L = 76,
    M = 77,
    N = 78,
    O = 79,
    P = 80,
    Q = 81,
    R = 82,
    S = 83,
    T = 84,
    V = 85,
    U = 86,
    W = 87,
    X = 88,
    Y = 89,
    Z = 90,
}
export class Renderer {
    public context: CanvasRenderingContext2D;
    public canvas: HTMLCanvasElement;
    public ui: HTMLDivElement;
    constructor(public container: HTMLElement, public width: number, public height: number) {
        this.canvas = document.createElement("canvas");
        this.context = this.canvas.getContext("2d");
        this.canvas.width = width;
        this.canvas.height = height;
        this.canvas.style.position = "absolute";
        container.appendChild(this.canvas);
    }
    public DebugDraw(fps: number) {
        this.context.font = "16px Arial";
        this.context.fillText(fps.toPrecision(2).toString(), 10, 15);
        console.log(fps);
    }
    public Clear() {
        this.context.clearRect(0, 0, 2000,2000);
    }
} 
export class Sprite extends Entity {
    public image:HTMLImageElement;
    public updateEvents:Array<()=> any>;
    constructor(position: Vector2, size: Vector2) {
        super(position, size);
        this.updateEvents = [];
    }
    public LoadGrapic(location: string) {
        this.image = document.createElement("img");
        this.image.src = location;
        console.log(this.image.width);
    }
    public Update() {
        this.updateEvents.forEach((f) => {
            f();
        });
    }
    public Draw(graphics: CanvasRenderingContext2D) {
        graphics.drawImage(this.image, 32, 0, 32, 32, this.position.x, this.position.y, 32, 32);
    }
}
export class State {
    public children: Array<Entity>;
    constructor() {
        this.children = new Array<Entity>();
    }
    public AddChild(child: Entity) {
        this.children.push(child);
    }
    public RemoveChild(child: Entity) {
        var m = this.children.lastIndexOf(child);
        delete this.children[m];
    }
    public Create() {
        throw new Error("ABSTRACT");
    }
    public Update() {
        this.children.forEach((c) => {
            c.Update();
        });
    }
    public OnClick(event: MouseEvent) {
        this.children.forEach((c) => {
            c.OnClick(event);
        });
    }
    public Draw(context: CanvasRenderingContext2D) {
        this.children.forEach((c) => {
            c.Draw(context);
        });
    }
}
export class MathHelper {
    public TAU = Math.PI * 2; 
    static Lerp(a: number, b: number, t: number): number {
        return a + (b - a) * t;
    }
}
export class Matrix2X2 {
    public values: Array<Array<number>>;
    constructor() {
        this.values = new Array<Array<number>>(2);
    }
    public static RotaionMatrix(theta: number): Matrix2X2 {
        var retval = new Matrix2X2();
        var m0 = Math.cos(theta);
        var m2 = Math.sin(theta);
        var m1 = -m2;
        retval.values[0] = new Array(m0, m1);
        retval.values[1] = new Array(m2, m0);
        return retval;
    }
} 
export class Vector2 {
    private length: number = null;
    private normal:Vector2;
    constructor(public x: number, public y: number) {
    }
    public Add(v: Vector2) {
        return new Vector2(this.x + v.x, this.y + v.y);
    }
    public Subtract(v: Vector2) {
        return new Vector2(this.x - v.x, this.y - v.y);
    }
    public MultiplyScalar(n: number) {
        return new Vector2(this.x * n, this.y * n);
    }
    public DivideScalar(n: number) {
        return new Vector2(this.x / n, this.y / n);
    }
    public MultiplyVector(v: Vector2) {
        return new Vector2(this.x * v.x, this.y * v.y);
    }
    public Rotate(theta: number) {
        var cosT = Math.cos(theta);
        var sinT = Math.sin(theta);
        var x = this.x * cosT - this.y * sinT;
        var y = this.x * sinT + this.y * cosT;
        return new Vector2(x, y);
    }
    public Length() {
        if (this.length != null) return this.length;
        this.length = (Math.sqrt(this.x * this.x + this.y * this.y));
        return this.length;
    }
    public MultiplyMatrix(matrix: Matrix2X2) {
        return new Vector2(this.x * matrix.values[0][0] - this.y * -matrix.values[0][1], this.x * matrix.values[1][0] + this.y * matrix.values[1][1]);
    }
    public toString() {
        return "[" + this.x + ", " + this.y + "]";
    }
    public Normalized() {
        return this.DivideScalar(this.Length());
    }
    public Copy() {
        return new Vector2(this.x,this.y);
    }
    public static Zero: Vector2 = new Vector2(0,0);
    public static Up: Vector2 = new Vector2(0,1);
    public static Right: Vector2 = new Vector2(1,0);
}
export class Frame {
    public ui: HTMLDivElement;
    public width: number;
    public height: number;
    constructor(public container: HTMLElement) {
        this.ui = document.createElement("div");
        this.width = container.offsetWidth;
        this.height = container.offsetHeight;
    }
    public AppendChild(child: HTMLElement) {
        this.ui.appendChild(child);
        this.ui.style.height = (this.ui.offsetHeight + child.offsetHeight + 5) + "px";
    }
    public AppendAt(index: number, child: HTMLElement) {
        this.ui.insertBefore(child, this.ui.children.item(index));
        this.ui.style.height = (this.ui.offsetHeight + child.offsetHeight + 5) + "px";
    }
    public Show() {
        this.container.appendChild(this.ui);
    }
    public Hide() {
        this.container.removeChild(this.ui);
    }
    public AddFrame() {
        var frame = new Frame(this.ui);
        return frame;
    }
    public AddButtonAt(index: number, text: string, callback: (ev: MouseEvent) => any) {
        var b = document.createElement("button");
        b.className = "UIElement";
        b.style.position = "relative";
        b.textContent = text;
        b.onclick = callback.bind(this);
        b.style.width = "100%";
        this.AppendAt(index, b);
    }
    public AddButton(text: string, callback: (ev: MouseEvent) => any) {
        var b = document.createElement("button");
        b.className = "UIElement";
        b.style.position = "relative";
        b.textContent = text;
        b.onclick = callback.bind(this);
        b.style.width = "100%";
        this.AppendChild(b);
    }
    public AddNumberInput(labelText: string, defaultNumber: number, onChangeCallback: (ev: Event) => any) {
        var uiDiv = document.createElement("div");
        var label = document.createElement("label");
        var input = document.createElement("input");
        label.textContent = labelText;
        input.type = "number";
        input.value = defaultNumber.toString();
        input.style.height = uiDiv.style.height;
        input.onchange = onChangeCallback.bind(this);
        uiDiv.className = input.className = "UIElement";
        uiDiv.appendChild(label);
        uiDiv.appendChild(input);
        this.AppendChild(uiDiv);
    }
    public AddInput(type: string, labelText: string, defaultValue: string, onChangeCallback: (ev: Event) => any) {
        var uiDiv = document.createElement("div");
        var label = document.createElement("label");
        var input = document.createElement("input");
        label.textContent = labelText;
        input.type = type;
        input.value = defaultValue;
        input.onchange = onChangeCallback.bind(this);
        uiDiv.className = input.className = "UIElement";
        uiDiv.appendChild(label);
        uiDiv.appendChild(input);
        this.AppendChild(uiDiv);
    }
    public AddLabel(labelText: string) {
        var div = document.createElement("div");
        var label = document.createElement("label");
        label.textContent = labelText;
        div.appendChild(label);
        div.className = "UIElement";
        return div;
    }
    public AddDropdownList(labelText:string, values:string[]):HTMLSelectElement {
        var select = document.createElement("select");
        select.className = "UIElement";
        for (var i = 0; i < values.length; i++) {
            var option = document.createElement("option");
            option.value = values[i];
            option.text = option.value;
            select.add(option);
        }
        var div = this.AddLabel(labelText);
        div.appendChild(select);
        this.AppendChild(div);
        return select;
    }
} 
export class SilicaFrame extends Frame{
    public ui: HTMLDivElement;
    public open: boolean;
    private collapseButton: HTMLButtonElement;
    private openPosition: number;
    private closedPostion: number;
    private animID: number;
    constructor(public container: HTMLElement){
        super(container);
        this.ui.className = "UIFrame";
        this.open = true;
        var collapseButton = this.collapseButton = document.createElement("button");
        collapseButton.className = "UIElement";
        collapseButton.style.position = "absolute";
        collapseButton.style.width = "100%";
        collapseButton.style.height = (this.height / 25).toString() + "px";
        collapseButton.style.bottom = "0";
        collapseButton.textContent = "Close UI";
        this.ui.appendChild(collapseButton);
        collapseButton.onclick = this.OpenClose.bind(this);
        this.openPosition = this.ui.offsetTop;
        this.closedPostion = this.openPosition - collapseButton.offsetTop;
        this.Show();
    }
    public AppendChild(child: HTMLElement) {
        super.AppendChild(child);
        this.closedPostion = this.openPosition - this.collapseButton.offsetTop;
    }
    private time: number = 0;
    public OpenFrame() {
        this.ui.style.top = MathHelper.Lerp(this.closedPostion, this.openPosition, this.time) + "px";
        if (this.time >= 1) {
            cancelAnimationFrame(this.animID);
            this.time = 0;
            this.open = true;
        } else {
            this.time += .2;
            this.animID = requestAnimationFrame(this.OpenFrame.bind(this));
        }
    }
    public CloseFrame() {
        this.ui.style.top = MathHelper.Lerp(this.openPosition, this.closedPostion, this.time) + "px";
        if (this.time >= 1) {
            cancelAnimationFrame(this.animID);
            this.time = 0;
            this.open = false;
        } else {
            this.time += .2;
            this.animID = requestAnimationFrame(this.CloseFrame.bind(this));
        }
    }
    public OpenClose(ev: MouseEvent) {
        if (this.open) {
            this.animID = requestAnimationFrame(this.CloseFrame.bind(this));
            this.collapseButton.textContent = "Open UI";
        } else {
            this.animID = requestAnimationFrame(this.OpenFrame.bind(this));
            this.collapseButton.textContent = "Close UI";
        }
    }
} 
export class Window extends Frame {
    constructor(container: HTMLElement) {
        super(container);
        this.ui.className = "UIWindow";
        this.Show();
        var closeButton = document.createElement("button");
        closeButton.className = "UIElement";
        closeButton.style.position = "relative";
        closeButton.style.width = closeButton.style.height = (this.height / 40).toString() + "px";
        closeButton.style.left = "95%";
        closeButton.textContent = "X";
        closeButton.style.textAlign = "center";
        this.ui.appendChild(closeButton);
        closeButton.onclick = ((ev: MouseEvent) => {
            this.Hide();
        }).bind(this);
    }
}
