/// <reference path="../modules/require.d.ts" />
/// <reference path="AppMain.ts" />
require.config({
    
});

declare var App;
require(['AppMain'], 
    (main) => {
        App = new main.AppMain();
        App.Run();
}); 