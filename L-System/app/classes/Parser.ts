﻿import Turtle = require("classes/Turtle");
import Silica = require("lib/silica");
import Vector2 = Silica.Vector2;
export = Parser;

class Parser {

    private static rewriteSymbol: string = '=>';
    private static assignmentSymbol: string = '=';
    private static commandSymbol: string = ':';
    private static pipe: string = '|';

    public static Parse(command: string) : Turtle{
        var t = new Turtle(Vector2.Zero, Vector2.Zero, 0);
        var lines = command.split(';');

        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];
            if (line.search(Parser.rewriteSymbol) > 0) {
                var ops = line.split(Parser.rewriteSymbol, 2);
                t.AddRewriteRule(ops[0], ops[1]);
            }else if (line.search(Parser.assignmentSymbol)>0) {
                ops = line.split(Parser.assignmentSymbol, 2);
                switch (ops[0].toLowerCase()) {
                    case "angle":
                        t.SetAngle(+ops[1]);
                        break;
                    case "magnitude":
                        t.magnitude = t.originalMagnitude = +ops[1];
                        break;
                    case "multiplier":
                        t.magnitudeMultiplier = +ops[1];
                        break;
                    case "axiom":
                        t.axiom = ops[1];
                        break;
                    case "position":
                        var s = ops[1].split(',');
                        t.SetPosition(new Vector2(+s[0], +s[1]));
                        break;
                    case "heading":
                        s = ops[1].split(',');
                        t.SetHeading(new Vector2(+s[0], +s[1])); 
                        break;
                    default:
                        console.error("Unknown identifier " + ops[0]);
                        break;
                }
            } else if (line.search(Parser.commandSymbol) > 0) {
                ops = line.split(Parser.commandSymbol, 2);
                var commands = ops[1].split(Parser.pipe);
                var foo = this.Foo(commands, t);
                t.AddOperation(ops[0], foo);
            }
        }
        return t;
    }
    public static Foo(commands: string[], t:Turtle) {
        return () => { commands.forEach(c => t[c].call(t)); };
    }
    


}