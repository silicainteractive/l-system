var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "../lib/silica", "classes/Stack"], function (require, exports, SilicaTS, Stack) {
    var Entity = SilicaTS.Entity;
    var Vector2 = SilicaTS.Vector2;
    var Turtle = (function (_super) {
        __extends(Turtle, _super);
        function Turtle(position, heading, magnitude) {
            _super.call(this, position, SilicaTS.Vector2.Zero);
            this.position = position;
            this.heading = heading;
            this.magnitude = magnitude;
            this.operations = {};
            this.rewriteRules = {};
            this.thickness = 1.5;
            this.magnitudeMultiplier = .5;
            this.originalPos = position;
            this.originalHeading = heading;
            this.originalMagnitude = magnitude;
            this.lines = new Array();
            this.positionStack = new Stack();
            this.headingStack = new Stack();
        }
        Turtle.prototype.AddRewriteRule = function (lhd, rhd) {
            this.rewriteRules[lhd] = rhd;
        };
        Turtle.prototype.SetAngle = function (degrees) {
            this.angle = degrees * (Math.PI / 180);
        };
        Turtle.prototype.AddOperation = function (operator, operation) {
            this.operations[operator] = operation;
        };
        Turtle.prototype.Reset = function (includeMagnitude) {
            if (includeMagnitude === void 0) { includeMagnitude = false; }
            this.heading = this.originalHeading;
            this.position = this.originalPos;
            this.lines = new Array();
            this.positionStack = new Stack();
            this.headingStack = new Stack();
            if (!includeMagnitude)
                return;
            this.magnitude = this.originalMagnitude;
        };
        Turtle.prototype.ExecuteCommand = function (command) {
            this.Reset();
            for (var i = 0; i < command.length; i++) {
                this.operations[command[i]].call(this);
            }
            if (this.logCommand)
                console.log(command);
        };
        Turtle.prototype.Rewrite = function (depth, input) {
            if (input === void 0) { input = this.axiom; }
            if (depth == 0)
                return input;
            var retval = "";
            for (var i = 0; i < input.length; i++) {
                var c = input[i];
                var rewrite = this.rewriteRules[c];
                retval += rewrite != null ? rewrite : c;
            }
            return this.Rewrite(--depth, retval);
        };
        Turtle.prototype.Rotate = function (negative) {
            if (negative === void 0) { negative = false; }
            var theta = (negative) ? -this.angle : this.angle;
            var v = this.heading;
            var cos = Math.cos(theta);
            var sin = Math.sin(theta);
            var x = (v.x * cos - v.y * sin);
            var y = (v.x * sin + v.y * cos);
            this.heading = new Vector2(x, y).Normalized();
        };
        Turtle.prototype.RotateNeg = function () {
            this.Rotate(true);
        };
        Turtle.prototype.SetHeading = function (heading) {
            this.heading = this.originalHeading = heading;
        };
        Turtle.prototype.SetPosition = function (pos) {
            this.position = this.originalPos = pos;
        };
        Turtle.prototype.Push = function () {
            this.headingStack.Push(this.heading);
            this.positionStack.Push(this.position);
        };
        Turtle.prototype.Pop = function () {
            this.heading = this.headingStack.Pop();
            this.position = this.positionStack.Pop();
        };
        Turtle.prototype.Update = function () {
        };
        Turtle.prototype.Forward = function () {
            var mag = this.heading.MultiplyScalar(this.magnitude);
            var newpos = this.position.Add(mag);
            this.lines.push(new Line(this.position, newpos));
            this.position = newpos;
        };
        Turtle.prototype.Draw = function (context) {
            context.beginPath();
            context.strokeStyle = this.style;
            context.lineWidth = this.thickness;
            this.lines.forEach(function (l) {
                context.moveTo(l.start.x, l.start.y);
                context.lineTo(l.end.x, l.end.y);
            });
            context.stroke();
            context.closePath();
        };
        return Turtle;
    })(Entity);
    var Line = (function () {
        function Line(start, end) {
            this.start = start;
            this.end = end;
        }
        return Line;
    })();
    return Turtle;
});
//# sourceMappingURL=Turtle.js.map