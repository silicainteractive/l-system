﻿import SilicaTS = require("lib/silica");
import Parser = require("classes/Parser");

import Window = SilicaTS.Window;

export = TurtleEditorWindow;

class TurtleEditorWindow extends Window {
    constructor() {
        super(document.body);
        this.AddTextArea("Input");
    }
    
}