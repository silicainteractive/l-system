import SilicaTS = require("lib/silica");
import Turtle = require("classes/Turtle");
import Parser = require("classes/Parser");
import TurtleEditorWindow = require("classes/TurtleEditorWindow");

import Keys     = SilicaTS.Keys;
import Game     = SilicaTS.Game;
import State    = SilicaTS.State;
import Input    = SilicaTS.Input;
import Vector2  = SilicaTS.Vector2;
import Window = SilicaTS.Window;

export class AppMain {
    private s: State;
    private game: Game;
    private itt: number;

    public Run() {
        var s = this.s = new State();
        var game = this.game = new Game(document.body, s);
        game.stepDraw = true;
        game.Start();
        var t = BasicTurtles.PythagorasTree();
        s.AddChild(t);

        var itt = this.itt = 0;

        var commandString = t.Rewrite(itt);
        t.ExecuteCommand(commandString);
        game.Draw();

        { //UI Region
            game.ShowUI();

            game.Frame.AddButton("Next step", () => {
                t.magnitude *= t.magnitudeMultiplier;
                commandString = t.Rewrite(++itt);
                t.ExecuteCommand(commandString);
                game.Draw();
            });

            game.Frame.AddButton("Previous step", () => {
                if (itt > 0) {
                    t.magnitude /= t.magnitudeMultiplier;
                    commandString = t.Rewrite(--itt);
                    t.ExecuteCommand(commandString);
                }
                game.Draw();
            });
            var select = game.Frame.AddDropdownList("Prefabs", BasicTurtles.prefabNames);
            select.onchange = () => {
                s.RemoveChild(t);
                var temp = t;
                t = BasicTurtles.prefabTurtles[select.selectedIndex];
                t.logCommand = temp.logCommand;
                temp.Reset();
                delete temp;
                s.AddChild(t);
                itt = 0;
                commandString = t.Rewrite(itt);
                t.ExecuteCommand(commandString);
                game.Draw();
            };
            game.Frame.AddButton("Show editor", () => {
                
            });
            var textArea = game.Frame.AddTextArea("Command");
            
            game.Frame.AddButton("Execute Command", (ev) => {
                var foo = textArea.value.replace(/(\r\n|\n|\r|\s+)/g, "");
                
                s.RemoveChild(t);
                var temp = t;
                t = Parser.Parse(foo);
                t.logCommand = temp.logCommand;
                temp.Reset();
                delete temp;
                t.Reset(true);
                s.AddChild(t);
                itt = 0;
                commandString = t.Rewrite(itt);
                t.ExecuteCommand(commandString);
                game.Draw();
            });
            game.Frame.AddInput("checkbox", "Log result in console", "false", (ev) => {
                var target = <HTMLInputElement>ev.target;
                t.logCommand = target.checked;
            });

        }//UI Region

    }
    public SetNewTurtle(t:Turtle, newTurtle:Turtle) {
        this.s.RemoveChild(t);
        var temp = t;
        t = newTurtle;
        t.logCommand = temp.logCommand;
        temp.Reset();
        delete temp;
        t.Reset(true);
        this.s.AddChild(t);
        this.itt = 0;
        var commandString = t.Rewrite(this.itt);
        t.ExecuteCommand(commandString);
        this.game.Draw();
    }
    
}


export class BasicTurtles {
    public static prefabNames = ['Pythagoras Tree','Koch Snowflake', 'Sierpinski', 'Dragon Curve', 'Fractal Plant'];
    public static prefabTurtles = [BasicTurtles.PythagorasTree(), BasicTurtles.KochSnoflake(), BasicTurtles.SierpinskiTriangle(), BasicTurtles.DragonCurve(), BasicTurtles.FractalPlant()];

    public static PythagorasTree() {
        var t = new Turtle(new Vector2(450, 470), new Vector2(0, -1), 400);
        t.SetAngle(45);

        t.AddRewriteRule("0", "1[0]0");
        t.AddRewriteRule("1", "11");
        var op0 = () => {
            var mag = t.magnitude;
            t.magnitude *= .25;
            t.Forward();
            t.magnitude = mag;
        }
        t.AddOperation("0", op0);

        t.AddOperation("1", t.Forward);

        var op = () => {
            t.Push();
            t.Rotate(true);
        }
        var op2 = () => {
            t.Pop();
            t.Rotate();
        }
        t.AddOperation("[", op);
        t.AddOperation("]", op2);
        t.axiom = "0";
        return t;
    }

    public static KochSnoflake() {
        var t = new Turtle(new Vector2(250, 150), new Vector2(1, 0), 100);
        t.magnitudeMultiplier = .45;
        t.SetAngle(60);

        t.AddRewriteRule("F", "F-F++F-F");

        t.AddOperation("F", t.Forward);
        t.AddOperation("+", t.Rotate);
        t.AddOperation("-", () => { t.Rotate(true); });

        t.axiom = 'F++F++F';
        return t;

    }

    public static SierpinskiTriangle(){
        var t = new Turtle(new Vector2(250, 400), new Vector2(1, 0), 400);
        t.SetAngle(60);

        t.AddRewriteRule("A", "B-A-B");
        t.AddRewriteRule("B", "A+B+A");

        t.AddOperation('A', t.Forward);
        t.AddOperation('B', t.Forward);
        t.AddOperation('+', t.Rotate);
        t.AddOperation('-', () => { t.Rotate(true); });

        t.axiom = "A";
        return t;
    }

    public static DragonCurve() {
        var t = new Turtle(new Vector2(400, 300), new Vector2(1, 0), 250);
        t.magnitudeMultiplier = .7;

        t.SetAngle(90);

        t.AddRewriteRule('X', "X+YF+");
        t.AddRewriteRule('Y', "-FX-Y");

        t.AddOperation('X', () => { return null; });
        t.AddOperation('Y', () => { return null; });
        t.AddOperation('F', t.Forward);
        t.AddOperation('+', t.Rotate);
        t.AddOperation('-', () => { t.Rotate(true); });

        t.axiom = "FX";
        return t;
    }

    public static FractalPlant() {
        var t = new Turtle(new Vector2(100, 470), new Vector2(0.5, -0.5), 100);
        t.thickness = 1;
        t.SetAngle(25);
        t.style = "black";

        t.AddRewriteRule("X", "F-[[X]+X]+F[+FX]-X");
        t.AddRewriteRule("F", "FF");

        t.AddOperation('X', () => { return; });
        t.AddOperation('F', t.Forward);
        t.AddOperation('[', t.Push);
        t.AddOperation(']', t.Pop);
        t.AddOperation('+', t.Rotate);
        t.AddOperation("-", () => { t.Rotate(true); });

        t.axiom = "F-[[X]+X]+F[+FX]-X";
        return t;

    }
}


